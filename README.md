# Keycloak Admin Java API Demo
Este proyecto continene un ejemplo de uso del API en Java de Keycloak para la administración de un realm. 
El API permite gestionar los realms, roles y usuarios. 

# Configuración de Keycloak
Para el ejemplo se necesita configurar un realm en Keycloak donde se conectará la aplicación para gestionar el realm.
 1. Ingresar al admin de Keycloak
 1. Ir al realm que se quiere configurar.
 1. Agregar un client nuevo (que vamos a usar para conectarnos y administrar todo el realm, en este ejemplo "negocios-del-barrio-realm-management")
 1. En el client, configurar:
       - Access Type: Confidential
       - Standard Flow Enabled: OFF
       - Direct Access Grants Enabled: OFF
       - Service Accounts Enabled: ON
 1. En la solapa "Service Account Roles" seleccionar el Client Roles "realm-management" y agregar los roles:
       - manage-realm
       - manage-clients
       - manage-users
 1. En la solapa "Credentials" copiar el secret y usarlo. 

Mas info sobre [Configuracion de Keycloak y creacion del realm de administracion](https://codersee.com/how-to-set-up-keycloak-admin-client-with-spring-boot-and-kotlin).

# Configuración en Java
Es necesario agregar la librería del API de Java al proyecto (ver pom.xml)
