package com.dosideas.keycloak;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.EventRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

/**
 * Para configurar un realm de administracion en Keycloak:
 *
 * 1. Ingresar al admin de Keycloak
 * 2. Ir al realm que se quiere configurar.
 * 3. Agregar un client nuevo (que vamos a usar para conectarnos y administrar todo el realm, en este ejemplo "negocios-del-barrio-realm-management")
 * 4. En el client, configurar:
 *     - Access Type: Confidential
 *     - Standard Flow Enabled: OFF
 *     - Direct Access Grants Enabled: OFF
 *     - Service Accounts Enabled: ON
 * 5. En la solapa "Service Account Roles" seleccionar el Client Roles "realm-management" y agregar los roles:
 *     - manage-clients, manage-realm, manage-users
 * 6. En la solapa "Credentials" copiar el secret y usarlo.
 *
 * Mas info sobre configuracion de Keycloak y creacion del realm de administracion:
 * https://codersee.com/how-to-set-up-keycloak-admin-client-with-spring-boot-and-kotlin/
 */
public class Main {

    public static void main(String[] args) {
        String serverUrl = "http://localhost:8080/auth";
        String realm = "negocios-del-barrio";
        String clientId = "Negocios-del-barrio-realm-management";
        String clientSecret = "3559b1bd-f2d9-45ee-9f51-cb8215b208af";

        Keycloak keycloak = KeycloakBuilder.builder()
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .serverUrl(serverUrl)
                .realm(realm)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build();


//        crearUsuario(keycloak, realm);
        buscarUsuario(keycloak, realm);
    }

    private static void crearUsuario(Keycloak keycloak, String realm) {
        System.out.println("Creando usuario");
        UserRepresentation user = crearUserRepresentation("ldeseta2@gmail.com");
        int status = keycloak.realm(realm).users().create(user).getStatus();
        System.out.println("HTTP Status: " + status);
    }

    private static UserRepresentation crearUserRepresentation(String email) {
        CredentialRepresentation credencial = new CredentialRepresentation();
        credencial.setTemporary(true);
        credencial.setType(CredentialRepresentation.PASSWORD);
        credencial.setValue("mi_password");

        UserRepresentation user = new UserRepresentation();
        user.setUsername(email);
        user.setEmail(email);
        user.setFirstName("Nombre");
        user.setLastName("Apellido");
        user.setEnabled(true);
        user.setCredentials(List.of(credencial));
        user.singleAttribute("empresaId", "123456");

        return user;
    }

    private static void buscarUsuario(Keycloak keycloak, String realm) {
        System.out.println("Buscando usuario...");
        List<UserRepresentation> users = keycloak.realm(realm).users().search("ldeseta@gmail.com");

        System.out.println("Cantidad de usuarios encontrada: " + users.size());
        UserRepresentation user = users.get(0);
        System.out.println(user.getId());
        System.out.println(user.getUsername());

        //actualizar atributo
        user.singleAttribute("atributoPrueba", "valor de prueba");
        keycloak.realm(realm).users().get(user.getId()).update(user);

        System.out.println("Atributos: ");
        System.out.println(user.getAttributes());

        System.out.println("Eventos:");
        List<String> type = Arrays.asList("LOGIN");
        String userId = user.getId();
        List<EventRepresentation> events = keycloak.realm(realm).getEvents(type, null, userId, null, null, null, null, null);
        events.forEach(e -> System.out.println(formatTime(e.getTime())));
    }

    private static String formatTime(long timeInMillis) {
        return LocalDateTime.ofEpochSecond(timeInMillis / 1000, 0, ZoneOffset.ofHours(-3)).toString();
    }

}
